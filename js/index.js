$(function(){
  $("[data-toggle='tooltip']").tooltip();
  $("[data-toggle='popover']").popover();
  $('.carousel').carousel({
    inter: 2000 
  });
  $('#contacto').on('show.bs.modal', function(e){console.log('el modal contacto se esta mostrando')
    $('#contactoBtn').removeClass('btn-outline-success');
    $('#contactoBtn').addClass('btn-default');          
  });
  $('#contacto').on('shown.bs.modal', function(e){console.log('el modal contacto se mostró')
  });
  $('#contacto').on('hide.bs.modal', function(e){console.log('el modal contacto se oculta')
  });
  $('#contacto').on('hiden.bs.modal', function(e){console.log('el modal contacto se ocultó')
  });
});     